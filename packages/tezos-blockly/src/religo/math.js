/**
 * @license
 * Copyright 2012 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @fileoverview Generating ReLIGO for math blocks.
 * @author q.neutron@gmail.com (Quynh Neutron)
 */
'use strict';
export default (Cls) => { 
	return class extends Cls {
		math_number(block) {
			// Numeric value.
			var code = Number(block.getFieldValue('NUM'));
			var order = code >= 0 ? this.ORDER_ATOMIC :
				this.ORDER_UNARY_NEGATION;
			return [code, order];
		}

		math_arithmetic(block) {
			// Basic arithmetic operators, and power.
			var OPERATORS = {
				'ADD': [' + ', this.ORDER_ADDITION],
				'MINUS': [' - ', this.ORDER_SUBTRACTION],
				'MULTIPLY': [' * ', this.ORDER_MULTIPLICATION],
				'DIVIDE': [' / ', this.ORDER_DIVISION],
				'POWER': [null, this.ORDER_COMMA]  // Handle power separately.
			};
			var tuple = OPERATORS[block.getFieldValue('OP')];
			var operator = tuple[0];
			var order = tuple[1];
			var argument0 = this.valueToCode(block, 'A', order) || '0';
			var argument1 = this.valueToCode(block, 'B', order) || '0';
			var code;
			// Power in ReLIGO requires a special case since it has no operator.
			if (!operator) {
				code = 'Math.pow(' + argument0 + ', ' + argument1 + ')';
				return [code, this.ORDER_FUNCTION_CALL];
			}
			code = argument0 + operator + argument1;
			return [code, order];
		}

		math_single(block) {
			// Math operators with single operand.
			var operator = block.getFieldValue('OP');
			var code;
			var arg;
			if (operator == 'NEG') {
				// Negation is a special case given its different operator precedence.
				arg = this.valueToCode(block, 'NUM',
					this.ORDER_UNARY_NEGATION) || '0';
				if (arg[0] == '-') {
					// --3 is not legal in JS.
					arg = ' ' + arg;
				}
				code = '-' + arg;
				return [code, this.ORDER_UNARY_NEGATION];
			}
			if (operator == 'SIN' || operator == 'COS' || operator == 'TAN') {
				arg = this.valueToCode(block, 'NUM',
					this.ORDER_DIVISION) || '0';
			} else {
				arg = this.valueToCode(block, 'NUM',
					this.ORDER_NONE) || '0';
			}
			// First, handle cases which generate values that don't need parentheses
			// wrapping the code.
			switch (operator) {
				case 'ABS':
					code = 'Math.abs(' + arg + ')';
					break;
				case 'ROOT':
					code = 'Math.sqrt(' + arg + ')';
					break;
				case 'LN':
					code = 'Math.log(' + arg + ')';
					break;
				case 'EXP':
					code = 'Math.exp(' + arg + ')';
					break;
				case 'POW10':
					code = 'Math.pow(10,' + arg + ')';
					break;
				case 'ROUND':
					code = 'Math.round(' + arg + ')';
					break;
				case 'ROUNDUP':
					code = 'Math.ceil(' + arg + ')';
					break;
				case 'ROUNDDOWN':
					code = 'Math.floor(' + arg + ')';
					break;
				case 'SIN':
					code = 'Math.sin(' + arg + ' / 180 * Math.PI)';
					break;
				case 'COS':
					code = 'Math.cos(' + arg + ' / 180 * Math.PI)';
					break;
				case 'TAN':
					code = 'Math.tan(' + arg + ' / 180 * Math.PI)';
					break;
			}
			if (code) {
				return [code, this.ORDER_FUNCTION_CALL];
			}
			// Second, handle cases which generate values that may need parentheses
			// wrapping the code.
			switch (operator) {
				case 'LOG10':
					code = 'Math.log(' + arg + ') / Math.log(10)';
					break;
				case 'ASIN':
					code = 'Math.asin(' + arg + ') / Math.PI * 180';
					break;
				case 'ACOS':
					code = 'Math.acos(' + arg + ') / Math.PI * 180';
					break;
				case 'ATAN':
					code = 'Math.atan(' + arg + ') / Math.PI * 180';
					break;
				default:
					throw Error('Unknown math operator: ' + operator);
			}
			return [code, this.ORDER_DIVISION];
		};

		math_constant(block) {
			// Constants: PI, E, the Golden Ratio, sqrt(2), 1/sqrt(2), INFINITY.
			var CONSTANTS = {
				'PI': ['Math.PI', this.ORDER_MEMBER],
				'E': ['Math.E', this.ORDER_MEMBER],
				'GOLDEN_RATIO':
				['(1 + Math.sqrt(5)) / 2', this.ORDER_DIVISION],
				'SQRT2': ['Math.SQRT2', this.ORDER_MEMBER],
				'SQRT1_2': ['Math.SQRT1_2', this.ORDER_MEMBER],
				'INFINITY': ['Infinity', this.ORDER_ATOMIC]
			};
			return CONSTANTS[block.getFieldValue('CONSTANT')];
		};

		math_number_property(block) {
			// Check if a number is even, odd, prime, whole, positive, or negative
			// or if it is divisible by certain number. Returns true or false.
			var number_to_check = this.valueToCode(block, 'NUMBER_TO_CHECK',
				this.ORDER_MODULUS) || '0';
			var dropdown_property = block.getFieldValue('PROPERTY');
			var code;
			if (dropdown_property == 'PRIME') {
				// Prime is a special case as it is not a one-liner test.
				var functionName = this.provideFunction_(
					'mathIsPrime',
					['function ' + this.FUNCTION_NAME_PLACEHOLDER_ + '(n) {',
						'  // https://en.wikipedia.org/wiki/Primality_test#Naive_methods',
						'  if (n == 2 || n == 3) {',
						'    return true;',
						'  }',
						'  // False if n is NaN, negative, is 1, or not whole.',
						'  // And false if n is divisible by 2 or 3.',
						'  if (isNaN(n) || n <= 1 || n % 1 != 0 || n % 2 == 0 ||' +
						' n % 3 == 0) {',
						'    return false;',
						'  }',
						'  // Check all the numbers of form 6k +/- 1, up to sqrt(n).',
						'  for (var x = 6; x <= Math.sqrt(n) + 1; x += 6) {',
						'    if (n % (x - 1) == 0 || n % (x + 1) == 0) {',
						'      return false;',
						'    }',
						'  }',
						'  return true;',
						'}']);
				code = functionName + '(' + number_to_check + ')';
				return [code, this.ORDER_FUNCTION_CALL];
			}
			switch (dropdown_property) {
				case 'EVEN':
					code = number_to_check + ' % 2 == 0';
					break;
				case 'ODD':
					code = number_to_check + ' % 2 == 1';
					break;
				case 'WHOLE':
					code = number_to_check + ' % 1 == 0';
					break;
				case 'POSITIVE':
					code = number_to_check + ' > 0';
					break;
				case 'NEGATIVE':
					code = number_to_check + ' < 0';
					break;
				case 'DIVISIBLE_BY':
					var divisor = this.valueToCode(block, 'DIVISOR',
						this.ORDER_MODULUS) || '0';
					code = number_to_check + ' % ' + divisor + ' == 0';
					break;
			}
			return [code, this.ORDER_EQUALITY];
		};

		math_change(block) {
			// Add to a variable in place.
			var argument0 = this.valueToCode(block, 'DELTA',
				this.ORDER_ADDITION) || '0';
			var varName = this.variableDB_.getName(
				block.getFieldValue('VAR'), Blockly.VARIABLE_CATEGORY_NAME);
			return varName + ' = (typeof ' + varName + ' == \'number\' ? ' + varName +
				' : 0) + ' + argument0 + ';\n';
		}

		math_on_list(block) {
			// Math functions for lists.
			var func = block.getFieldValue('OP');
			var list, code;
			switch (func) {
				case 'SUM':
					list = this.valueToCode(block, 'LIST',
						this.ORDER_MEMBER) || '[]';
					code = list + '.reduce(function(x, y) {return x + y;})';
					break;
				case 'MIN':
					list = this.valueToCode(block, 'LIST',
						this.ORDER_COMMA) || '[]';
					code = 'Math.min.apply(null, ' + list + ')';
					break;
				case 'MAX':
					list = this.valueToCode(block, 'LIST',
						this.ORDER_COMMA) || '[]';
					code = 'Math.max.apply(null, ' + list + ')';
					break;
				case 'AVERAGE':
					// mathMean([null,null,1,3]) == 2.0.
					var functionName = this.provideFunction_(
						'mathMean',
						['function ' + this.FUNCTION_NAME_PLACEHOLDER_ +
							'(myList) {',
							'  return myList.reduce(function(x, y) {return x + y;}) / ' +
							'myList.length;',
							'}']);
					list = this.valueToCode(block, 'LIST',
						this.ORDER_NONE) || '[]';
					code = functionName + '(' + list + ')';
					break;
				case 'MEDIAN':
					// mathMedian([null,null,1,3]) == 2.0.
					var functionName = this.provideFunction_(
						'mathMedian',
						['function ' + this.FUNCTION_NAME_PLACEHOLDER_ +
							'(myList) {',
							'  var localList = myList.filter(function (x) ' +
							'{return typeof x == \'number\';});',
							'  if (!localList.length) return null;',
							'  localList.sort(function(a, b) {return b - a;});',
							'  if (localList.length % 2 == 0) {',
							'    return (localList[localList.length / 2 - 1] + ' +
							'localList[localList.length / 2]) / 2;',
							'  } else {',
							'    return localList[(localList.length - 1) / 2];',
							'  }',
							'}']);
					list = this.valueToCode(block, 'LIST',
						this.ORDER_NONE) || '[]';
					code = functionName + '(' + list + ')';
					break;
				case 'MODE':
					// As a list of numbers can contain more than one mode,
					// the returned result is provided as an array.
					// Mode of [3, 'x', 'x', 1, 1, 2, '3'] -> ['x', 1].
					var functionName = this.provideFunction_(
						'mathModes',
						['function ' + this.FUNCTION_NAME_PLACEHOLDER_ +
							'(values) {',
							'  var modes = [];',
							'  var counts = [];',
							'  var maxCount = 0;',
							'  for (var i = 0; i < values.length; i++) {',
							'    var value = values[i];',
							'    var found = false;',
							'    var thisCount;',
							'    for (var j = 0; j < counts.length; j++) {',
							'      if (counts[j][0] === value) {',
							'        thisCount = ++counts[j][1];',
							'        found = true;',
							'        break;',
							'      }',
							'    }',
							'    if (!found) {',
							'      counts.push([value, 1]);',
							'      thisCount = 1;',
							'    }',
							'    maxCount = Math.max(thisCount, maxCount);',
							'  }',
							'  for (var j = 0; j < counts.length; j++) {',
							'    if (counts[j][1] == maxCount) {',
							'        modes.push(counts[j][0]);',
							'    }',
							'  }',
							'  return modes;',
							'}']);
					list = this.valueToCode(block, 'LIST',
						this.ORDER_NONE) || '[]';
					code = functionName + '(' + list + ')';
					break;
				case 'STD_DEV':
					var functionName = this.provideFunction_(
						'mathStandardDeviation',
						['function ' + this.FUNCTION_NAME_PLACEHOLDER_ +
							'(numbers) {',
							'  var n = numbers.length;',
							'  if (!n) return null;',
							'  var mean = numbers.reduce(function(x, y) {return x + y;}) / n;',
							'  var variance = 0;',
							'  for (var j = 0; j < n; j++) {',
							'    variance += Math.pow(numbers[j] - mean, 2);',
							'  }',
							'  variance = variance / n;',
							'  return Math.sqrt(variance);',
							'}']);
					list = this.valueToCode(block, 'LIST',
						this.ORDER_NONE) || '[]';
					code = functionName + '(' + list + ')';
					break;
				case 'RANDOM':
					var functionName = this.provideFunction_(
						'mathRandomList',
						['function ' + this.FUNCTION_NAME_PLACEHOLDER_ +
							'(list) {',
							'  var x = Math.floor(Math.random() * list.length);',
							'  return list[x];',
							'}']);
					list = this.valueToCode(block, 'LIST',
						this.ORDER_NONE) || '[]';
					code = functionName + '(' + list + ')';
					break;
				default:
					throw Error('Unknown operator: ' + func);
			}
			return [code, this.ORDER_FUNCTION_CALL];
		}

		math_modulo(block) {
			// Remainder computation.
			var argument0 = this.valueToCode(block, 'DIVIDEND',
				this.ORDER_MODULUS) || '0';
			var argument1 = this.valueToCode(block, 'DIVISOR',
				this.ORDER_MODULUS) || '0';
			var code = argument0 + ' % ' + argument1;
			return [code, this.ORDER_MODULUS];
		}

		math_constrain(block) {
			// Constrain a number between two limits.
			var argument0 = this.valueToCode(block, 'VALUE',
				this.ORDER_COMMA) || '0';
			var argument1 = this.valueToCode(block, 'LOW',
				this.ORDER_COMMA) || '0';
			var argument2 = this.valueToCode(block, 'HIGH',
				this.ORDER_COMMA) || 'Infinity';
			var code = 'Math.min(Math.max(' + argument0 + ', ' + argument1 + '), ' +
				argument2 + ')';
			return [code, this.ORDER_FUNCTION_CALL];
		}
	}
}
