'use strict';

import ls from 'local-storage';
import * as Blockly from 'blockly';

function _restoreBlocks(workspace) {
    var blockly_local = ls.get('blockly-local');
    if (blockly_local) {        
        var xml = Blockly.Xml.textToDom(blockly_local);
        Blockly.Xml.domToWorkspace(xml, workspace);
    }
}

function setupStorage(workspace) {
    window.setTimeout(function(){ _restoreBlocks(workspace); }, 0);
    window.addEventListener('unload',
        function() {            
            var xml = Blockly.Xml.workspaceToDom(workspace);
            ls.set('blockly-local', Blockly.Xml.domToText(xml));			
        }, false);
}

export default setupStorage;